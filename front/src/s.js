/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { Tabs, NoSsr, Tab, Typography, AppBar, withStyles, Grid, Button } from '@material-ui/core';
import AppBarBottom from './components/AppBarBottom';
import SignIn from './components/intro/SignIn';
import Portfolio from './components/Portfolio';
import Profile from './components/Profile';
import styles from './jss/Tab';
import Gallery2D from './Gallery2D';
import character from './img/character.jpg';
import { withBook } from './context/Context';
import Controls from './Carouselcontrols';
import Swipeable from 'react-swipeable';

const IMAGES = [0, 1, 2];

const RIGHT = -1;
const LEFT = 1;
const numbers = [
	{ id: 0, img: { character }, title: 'Portfolio' },
	{ id: 1, logo: { character }, title: 'Portfolio' },
	{ id: 2, logo: { character }, title: 'Portfolio' }
];
function TabContainer(props) {
	return (
		<Typography component="div" style={{ padding: 8 * 3 }}>
			{props.children}
		</Typography>
	);
}

TabContainer.propTypes = {
	children: PropTypes.node.isRequired
};

function LinkTab(props) {
	return <Tab component="a" onClick={event => event.preventDefault()} {...props} />;
}

class NavTabs extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = { imageIdx: 0, tabs: 0 };
	}
	onSwiped(direction) {
		console.log(this.state.imageIdx);
		const change = direction === RIGHT ? RIGHT : LEFT;
		const adjustedIdx = this.state.imageIdx + Number(change);
		let newIdx;
		if (adjustedIdx >= IMAGES.length) {
			newIdx = 0;
		} else if (adjustedIdx < 0) {
			newIdx = IMAGES.length - 1;
		} else {
			newIdx = adjustedIdx;
		}
		this.setState({ imageIdx: newIdx });
	}
	render() {
		const { classes } = this.props;
		const { tabs } = this.props;

		return (
			<NoSsr>
				<AppBarBottom key={1} />

				<div className={classes.root} key={2}>
					<Swipeable
						className="swipe"
						trackMouse
						style={{ touchAction: 'none' }}
						preventDefaultTouchmoveEvent
						onSwipedLeft={() => this.onSwiped(LEFT)}
						onSwipedRight={() => this.onSwiped(RIGHT)}
					>
						{/* <AppBar position="static">
						<Tabs variant="fullWidth" value={value} onChange={this.handleChange}>
							<LinkTab label="Page One" href="page1" />
							<LinkTab label="Page Two" href="page2" />
							<LinkTab label="Page Three" href="page3" />
						</Tabs>
					</AppBar> */}
						{tabs === 0 && (
							<TabContainer key={1}>
								<Portfolio />
							</TabContainer>
						)}
						{tabs === 1 && (
							<TabContainer key={2}>
								<Profile />
							</TabContainer>
						)}
						{tabs === 2 && (
							<TabContainer key={3}>
								<Gallery2D />
							</TabContainer>
						)}
					</Swipeable>
				</div>
				<Button onClick={() => this.onSwiped(RIGHT)} className={classes.floatLeft}>
					⇦
				</Button>
				<Button onClick={() => this.onSwiped(LEFT)} className={classes.floatRight}>
					⇨
				</Button>
				<Controls itemsnumber={numbers.length} />
			</NoSsr>
		);
	}
}

NavTabs.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withBook(withStyles(styles)(NavTabs));
