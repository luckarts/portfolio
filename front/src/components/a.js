import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';

import { CardMedia, CardHeader, CardContent, Typography } from '@material-ui/core';

/* eslint-disable */
const styles = theme => ({
	card: {
		width: '100%',
		backgroundColor: 'transparent',
		height: '100%',
		display: 'flex',
		flexDirection: 'column'
	},
	cardMedia: {
		width: '100%',
		height: '100%'
	},
	cardContent: {
		backgroundColor: '#eee',
		flexGrow: 1
	}
});
class MediaCard extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		const { classes } = this.props;
		return (
			<Card className={classes.card}>
				<CardMedia
					component="img"
					alt="Contemplative Reptile"
					className={classes.media}
					height="300"
					image={this.props.image}
					title="Contemplative Reptile"
				/>
				<CardContent>
					<Typography component="p">{this.props.title}</Typography>
					<Typography component="p">{this.props.detail}</Typography>
				</CardContent>
			</Card>
		);
	}
}

MediaCard.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MediaCard);
