const styles = theme => ({
	main: {
		backgroundSize: 'cover',
		height: '100%',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1,
		backgroundColor: '#e7e6e3;',
		marginTop: '70px',
		[theme.breakpoints.down('sm', 'xs')]: {
			marginBottom: '90px',
			marginTop: '0'
		}

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},

	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	title: {
		paddingTop: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,
		textAlign: 'center',
		fontSize: '2em',
		width: '100%',
		color: '#eee'
		//borderBottom: `1px solid ${theme.palette.grey[500]}`
	},
	summary: {
		marginTop: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,

		fontSize: '1em',
		width: '100%',
		color: '#eee',

		//	borderBottom: `1px solid ${theme.palette.grey[500]}`,
		paddingBottom: theme.spacing.unit * 5,
		[theme.breakpoints.down('sm', 'xs')]: {
			marginLeft: theme.spacing.unit * 2,
			marginRight: theme.spacing.unit * 2
		}
		//borderBottom: `1px solid ${theme.palette.grey[500]}`
	},
	h2: {
		textAlign: 'center',
		fontSize: '1em',
		//borderBottom: `1px solid ${theme.palette.grey[500]}`,
		paddingBottom: theme.spacing.unit * 5
	},

	h3: {
		paddingTop: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,
		fontSize: '2em',
		width: '100%',
		color: '#213142',
		fontWeight: 'bold'
	},
	h4: {
		paddingTop: theme.spacing.unit * 5,
		fontSize: '1.2em',
		width: '100%',
		color: '#213142'
	},
	h5: {
		marginTop: '2vh',
		fontSize: '1.4em',
		fontWeight: 'bold',
		width: '100%',
		color: '#eee'
	},
	h6: {
		marginTop: '2vh',
		fontSize: '1.2em',
		fontWeight: 'bold',
		width: '100%',
		color: '#eee'
	},
	chip: {
		margin: ' 10px',
		fontSize: '1.2em'
	},

	character: {
		marginTop: '12vh',
		width: '150px',
		height: '150px',
		borderRadius: '300px'
	},
	bg: {
		backgroundImage: `url('/img/forest2.jpg')`,
		backgroundSize: `cover`,
		backgroundPosition: `right`,

		width: '100%',
		textAlign: 'center',
		paddingBottom: theme.spacing.unit * 5
	},
	email: { marginBottom: '5vh', textDecoration: 'none' },
	card: { backgroundColor: '#fff', borderRadius: '10px' }
});
export default styles;
