import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import { CardContent, Typography, Button, Grid } from '@material-ui/core';

/* eslint-disable */
const styles = theme => ({
	card: {
		width: '100%',
		backgroundColor: '#fafafa;',
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
		borderRadius: '10px'
	},
	cardMedia: {
		width: '100%',
		height: '100%'
	},
	cardContent: {
		backgroundColor: '#eee',
		flexGrow: 1
	},
	h1: { align: 'left', fontWeight: 'bold' },
	p: { align: 'left' },
	bgCard0: { margin: '5vh' },
	bgCard1: { margin: '10vh' },
	bgCard2: { margin: '20vh' },
	test: { backgroundImage: `url('/img/forest2.jpg')` },
	link: { height: '100%' },
	button: { textDecoration: 'none' }
});
class MediaCard extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		const { classes } = this.props;
		return (
			<Card className={classes.card}>
				<a target="_blank" href={this.props.link} className={classes.button}>
					<CardContent
						style={{
							backgroundImage: `url(${this.props.image})`,
							height: '30vh',
							backgroundSize: 'cover',
							backgroundPosition: 'center'
						}}
					>
						<Grid
							container
							className={classes.link}
							direction="row"
							justify="flex-end"
							alignItems="flex-end"
						>
							<Button
								style={{
									backgroundColor: '#eee'
								}}
							>
								Web Site
								<i className="material-icons">trending_flat</i>{' '}
							</Button>
						</Grid>
					</CardContent>
				</a>
				<CardContent>
					<Typography align="left" className={classes.h1}>
						{this.props.title}
					</Typography>
					<Typography align="left" className={classes.p}>
						{this.props.detail}
					</Typography>
				</CardContent>
			</Card>
		);
	}
}

MediaCard.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MediaCard);
