import React, { Component } from 'react';
import AppBarBottom from './components/AppBarBottom';
import AppBar from './components/test/AppBar';
import Profil from './components/page1/Profiltest';
import Profile from './components/Profile';
import SkillList from './components/SkillList';
import CardList from './components/CardList';
import './App.css';
import { Grid, withStyles } from '@material-ui/core';
const styles = theme => ({
	container: {
		//backgroundImage: `url(${Background})`
		//backgroundColor: 'rgba(17, 17, 17)'
		backgroundColor: '#eee'
	}
});

class App extends Component {
	render() {
		const { classes } = this.props;
		return (
			<Grid container alignItems="center" className={classes.container}>
				<AppBarBottom />
				<Profile />

				<AppBar />
			</Grid>
		);
	}
}
export default withStyles(styles)(App);
