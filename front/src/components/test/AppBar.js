import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';

const styles = theme => ({
	text: {
		paddingTop: theme.spacing.unit * 2,
		paddingLeft: theme.spacing.unit * 2,
		paddingRight: theme.spacing.unit * 2
	},

	appBar: {
		top: 'auto',
		bottom: 0
	},
	toolbar: {
		alignItems: 'center',
		justifyContent: 'space-between'
	}
});

function BottomAppBar(props) {
	const { classes } = props;
	return (
		<div>
			<CssBaseline />

			<AppBar position="relative" color="primary" className={classes.appBar}>
				<Toolbar className={classes.toolbar} />
			</AppBar>
		</div>
	);
}

BottomAppBar.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(BottomAppBar);
