import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import CardPortfolio from './CardPortfolio';
import { withBook } from '../context/Context';
import styles from '../jss/Portfolio';
import '../jss/port.scss';
const Projets = [
	{
		id: 0,
		title: 'Calypso',
		image: '/img/YkariIntro.jpg',
		date: 'oct. 2018 – nov. 2018',
		detail: `Création d'un site responsive (+ application mobile)
communautaire autour d'un jeu (également développé par
l'équipe).`,
		link: 'https://www.ykari.com/'
	},
	{
		id: 1,
		title: 'Ikari',
		image: '/img/hikari.jpg',
		date: 'oct. 2018 – nov. 2018',
		detail: `Lors de la GameJam Epitech nous avons realisés ce jeu. Voici son histoire : Le monde d'Ikari se meurt , pour son village , il vous faudra ramener les Ikaris perdu dans le labyrinthe  afin d'y ramener la vie !
		Projet réalisé en deux jours.
`,
		link: 'http://188.166.107.102:5000/'
	},
	{
		id: 2,
		title: 'Spotter',
		image: '/img/Spotter.jpg',
		date: 'oct. 2018 – nov. 2018',
		detail: `Spotter est un jeu de cartes sur le thème Harry Potter , jouable de 2 à 4 joueur disponible sur Navigateur uniquement. Nous avons crée ce jeux lors d'un projet de la wild code school par cinq autres développeurs
`,
		link: 'https://luckarts.github.io/Spotter/#/'
	}
];
/* eslint-disable */
class SignIn extends Component {
	constructor(props) {
		super();
		this.state = {};
	}

	onClick = () => {
		console.log('hello');
	};
	render() {
		const { classes } = this.props;
		return (
			<Grid item container className={classes.main} justify="center">
				<Typography className={classes.title} variant="h6" noWrap>
					Projets
				</Typography>

				<Grid
					container
					className={classes.portfolio}
					item
					xs={12}
					spacing={40}
					sm={8}
					justify="center"
					direction="row"
				>
					{Projets.map((Projets, i) => (
						<Grid container item key={i} lg={12} xs={12}>
							{/* <Button
								value={Projets.id}
								key={Projets.toString()}
								onClick={() => {
									this.props.activeTab(4);
								}}
							> */}
							{/* <img src={character.image} alt={character.name} /> */}

							<CardPortfolio
								image={Projets.image}
								title={Projets.title}
								detail={Projets.detail}
								techno={Projets.techno}
								value={Projets.id}
								link={Projets.link}
							/>
						</Grid>
					))}
				</Grid>
			</Grid>
		);
	}
}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withBook(withStyles(styles)(SignIn));
