import React from 'react';
import ReactDOM from 'react-dom';
import './styless.css';
import Masonry from 'react-masonry-component';
import nature from '../img/study.jpg';
import ConceptStreet from '../img/concept_street_church1.jpg';
import Forest from '../img/forest2.jpg';
import paintingLyly from '../img/painting-lyly.jpg';
import River from '../img/painting_river_mark.jpg';
import Revenant from '../img/revenant.jpg';
import Portrait from '../img/study_color2.jpg';
import Tableau from '../img/tableau.png';
import Gallery from 'react-photo-gallery';
const Painting = [
	{
		image: nature,
		width: 4,
		height: 3
	},
	{
		image: ConceptStreet,
		width: 4,
		height: 3
	},

	{
		image: paintingLyly,
		width: 4,
		height: 3
	},
	{
		image: Forest,
		width: 4,
		height: 3
	},
	{
		image: River,
		width: 4,
		height: 3
	},
	{
		image: Revenant,
		width: 4,
		height: 3
	},
	{
		image: Portrait,
		width: 4,
		height: 3
	},
	{
		image: Tableau,
		width: 4,
		height: 3
	}
];

class Mansonry extends React.Component {
	render() {
		return <Gallery photos={Painting} direction={'column'} />;
	}
}

export default Mansonry;
