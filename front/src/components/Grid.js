import React, { Component } from 'react';
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';
import { Button } from '@material-ui/core';
import './Gallery.css';
import { withBook } from '../context/Context';
import Lightbox from 'react-images';
import Gallery from 'react-photo-gallery';
const Painting = [
	{
		image: '/img/study.jpg'
	},
	{
		image: '/img/concept_street_church1.jpg'
	},

	{
		image: '/img/painting-lyly.jpg'
	},
	{
		image: '/img/forest2.jpg'
	},
	{
		image: '/img/painting_river_mark.jpg'
	},
	{
		image: '/img/revenant.jpg'
	},

	{
		image: '/img/tableau.png'
	},
	{
		image: '/img/study_color2.jpg'
	}
];
const photos = [
	{
		src: '/img/study.jpg',
		width: 4,
		height: 3
	},
	{
		src: '/img/concept_street_church1.jpg',
		width: 1,
		height: 1
	},
	{
		src: '/img/forest2.jpg',
		width: 3,
		height: 4
	},
	{
		src: '/img/painting_river_mark.jpg',
		width: 3,
		height: 4
	},
	{
		src: '/img/revenant.jpg',
		width: 3,
		height: 4
	},
	{
		src: '/img/tableau.png',
		width: 4,
		height: 3
	},
	{
		src: '/img/study_color2.jpg',
		width: 3,
		height: 4
	}
];
class Imagenes extends Component {
	constructor() {
		super();
		this.state = {
			currentImage: 0
		};
		this.closeLightbox = this.closeLightbox.bind(this);
		this.openLightbox = this.openLightbox.bind(this);
		this.gotoNext = this.gotoNext.bind(this);
		this.gotoPrevious = this.gotoPrevious.bind(this);
	}
	openLightbox(event, obj) {
		this.setState({
			currentImage: obj.index,
			lightboxIsOpen: true
		});
	}
	closeLightbox() {
		this.setState({
			currentImage: 0,
			lightboxIsOpen: false
		});
	}
	gotoPrevious() {
		this.setState({
			currentImage: this.state.currentImage - 1
		});
	}
	gotoNext() {
		this.setState({
			currentImage: this.state.currentImage + 1
		});
	}
	render() {
		return (
			<div>
				<ResponsiveMasonry columnsCountBreakPoints={{ 350: 4, 750: 2, 900: 4 }}>
					<Masonry>
						{photos.map((photos, i) => (
							<div>
								<div style={{ padding: '7px' }}>
									<Button onClick={this.openLightbox}>
										<img
											key={i}
											src={photos.src}
											style={{
												width: '100%',
												display: 'block',
												marginBottom: '5px',
												borderRadius: '10px'
											}}
											alt="img"
										/>
									</Button>
								</div>
							</div>
						))}
					</Masonry>
				</ResponsiveMasonry>

				<Lightbox
					images={photos}
					currentImage={this.state.currentImage}
					isOpen={this.state.lightboxIsOpen}
					onClickPrev={this.gotoPrevLightboxImage}
					onClickNext={this.gotoNextLightboxImage}
					onClose={this.closeLightbox}
				/>
			</div>
		);
	}
}
export default withBook(Imagenes);
