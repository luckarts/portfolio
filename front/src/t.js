/* eslint-disable */
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import MapList from './components/MapList';
import DetailMap from './components/DetailMap';
import Auth from './components/Auth';
import Signup from './components/Signup';
import Game from './components/Game';
import CreatorProfile from './components/CreatorProfile';
import { PrivateRoute } from './components/PrivateRoute';
import PlayerProfile from './components/PlayerProfile';
import './css/App.scss';
import UserProvider from './context/UserContext';
import PlayerProvider from './context/PlayerContext';
import Member from './components/Member';
import MemberEdit from './components/MemberEdit';

class App extends Component {
	render() {
		return (
			<div className="App">
				<Switch>
					<Route exact path="/" component={Auth} />
					<Route exact path="/signup" component={Signup} />
					<PlayerProvider>
						<UserProvider>
							<PrivateRoute path="/maplist" component={MapList} />
							<PrivateRoute path="/map/:id" component={DetailMap} />
							<Route exact path="/Canvas" component={Game} />
							<PrivateRoute exact path="/playerprofile" component={PlayerProfile} />
							<PrivateRoute exact path="/CreatorProfile" component={CreatorProfile} />
							<PrivateRoute exact path="/member" component={Member} />
							<PrivateRoute exact path="/memberEdit" component={MemberEdit} />
						</UserProvider>
					</PlayerProvider>
				</Switch>
			</div>
		);
	}
}

export default App;
