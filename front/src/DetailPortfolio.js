import React, { Component } from 'react';
import AppBarBottom from './components/AppBarBottom';

import DetailPortolio from './components/DetailPortolio';

import './App.css';
import { Grid, withStyles } from '@material-ui/core';

const styles = theme => ({
	container: {
		//backgroundImage: `url(${Background})`
		//backgroundColor: 'rgba(17, 17, 17)'
		backgroundColor: '#eee'
	}
});

class App extends Component {
	render() {
		const { classes } = this.props;
		return (
			<Grid container alignItems="center" className={classes.container}>
				<AppBarBottom />
				<DetailPortolio />
			</Grid>
		);
	}
}
export default withStyles(styles)(App);
