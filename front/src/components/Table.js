//Languages & Frameworks
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import { Grid, TableRow, TableHead, TableCell, TableBody, Table } from '@material-ui/core';

const styles = theme => ({
	root: {
		marginBottom: theme.spacing.unit * 20,
		display: 'flex',
		backgroundColor: 'transparent',
		width: '100%',
		marginTop: theme.spacing.unit * 3,
		overflowX: 'auto'
	},
	table: {
		width: '50%'
	}
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
	id += 1;
	return { id, name, calories, fat, carbs, protein };
}

const rows = [
	createData('HTML / CSS / JS', 'Nodejs', ' Git / Gitlab', 24),
	createData('Bootstrap / Material ui', 'SQL', 'TDD', 37),
	createData('React / redux / contaxt', 'MongoDb', 'Agile', 24),
	createData('Angular', '', 'Scrum', 67),
	createData('Gingerbread', '', '', 49)
];

function SimpleTable(props) {
	const { classes } = props;

	return (
		<Grid container className={classes.root} justify="center" direction="row">
			<Table className={classes.table}>
				<TableHead>
					<TableRow>
						<TableCell>Front end</TableCell>
						<TableCell align="right">BACK-END</TableCell>
						<TableCell align="right">WORKFLOW </TableCell>
						<TableCell align="right">Design</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{rows.map(row => {
						return (
							<TableRow key={row.id}>
								<TableCell component="th" scope="row">
									{row.name}
								</TableCell>
								<TableCell align="right">{row.calories}</TableCell>
								<TableCell align="right">{row.fat}</TableCell>
								<TableCell align="right">{row.carbs}</TableCell>
								<TableCell align="right">{row.protein}</TableCell>
							</TableRow>
						);
					})}
				</TableBody>
			</Table>
		</Grid>
	);
}

SimpleTable.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleTable);
