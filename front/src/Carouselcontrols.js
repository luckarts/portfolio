import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import classNames from 'classnames';
import { withBook } from './context/Context';
const styles = theme => ({
	root: {
		display: 'flex',
		height: 40,
		bottom: 0,
		position: 'fixed',
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center'
	},
	item: {
		width: 15,
		height: 15,
		backgroundColor: theme.palette.background.paper,
		marginRight: 5
	},
	active: {
		width: 20,
		height: 20
	}
});
//const Carouselcontrols = ({ classes, itemsnumber, index, handleChangeIndex }) => {
class Carouselcontrols extends Component {
	constructor(props) {
		super();
	}
	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				{[...Array(this.props.itemsnumber).keys()].map((item, i) => (
					<Paper
						className={classNames({
							[classes.item]: true,
							[classes.active]: i === this.props.index
						})}
						key={i}
						elevation={12}
						onClick={() => {
							this.props.activeTab(i);
						}}
					/>
				))}
			</div>
		);
	}
}

export default withBook(withStyles(styles)(Carouselcontrols));
