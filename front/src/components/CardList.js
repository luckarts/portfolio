import React from 'react';
import PropTypes from 'prop-types';

import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Card from './Card';

const styles = theme => ({
	main: {
		backgroundColor: '#eee',
		height: '90vh',
		width: '100%',
		display: 'flex' // Fix IE 11 issue.

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	paper: {
		//marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	cardGrid: {
		marginTop: theme.spacing.unit * 8,
		alignItems: 'center',
		justifyContent: 'center'
	},
	title: {
		alignItems: 'center',
		marginTop: theme.spacing.unit * 20,
		textAlign: 'center',
		paddingTop: theme.spacing.unit * 8,
		paddingBottom: theme.spacing.unit * 8,
		justify: 'center',
		fontSize: '2em',

		color: '#000',
		borderBottom: `1px solid ${theme.palette.grey[500]}`
	}
});
const featuredCard = [
	{
		title: 'Featured post',
		date: 'Nov 12',
		description: 'This is a wider card with supporting text below as a natural lead-in to additional content.'
	},
	{
		title: 'Post title',
		date: 'Nov 11',
		description: 'This is a wider card with supporting text below as a natural lead-in to additional content.'
	},
	{
		title: 'Post title',
		date: 'Nov 11',
		description: 'This is a wider card with supporting text below as a natural lead-in to additional content.'
	}
];

function SignIn(props) {
	const { classes } = props;

	return (
		<Grid>
			<Typography
				className={classes.title}
				variant="subtitle1"
				align="center"
				color="textSecondary"
				component="p"
			>
				Portfolio
			</Typography>

			<Grid container spacing={40} className={classes.cardGrid}>
				{featuredCard.map(post => (
					<Grid container item key={featuredCard} sm={3} md={4} lg={3} xs={12} md={6}>
						<Card />
					</Grid>
				))}
			</Grid>
		</Grid>
	);
}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SignIn);
