import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Tabs from './Tabs';
import './jss/App.scss';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Context from './context/Context';

class App extends Component {
	render() {
		return (
			<div>
				<MuiThemeProvider>
					<Context>
						<Switch>
							<Route exact path="/" component={Tabs} />
						</Switch>
					</Context>
				</MuiThemeProvider>
			</div>
		);
	}
}
export default App;
