/*eslint-disable*/
import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import { Grid } from '@material-ui/core';
//import '../css/NavPartage.scss';
const styles = theme => ({
	icon: {
		width: '50px',
		height: 'auto',
		marginLeft: '1vw',
		marginRight: '1vw',
		backgroundColor: '#eee',
		borderRadius: '100px',
		padding: '5px',
		marginTop: '2vh'
	}
});

function NavPartage(props) {
	const { classes } = props;

	return (
		<Grid>
			<a target="_blank" href="https://www.linkedin.com/in/luc-bachelerie-developperjs">
				<img className={classes.icon} src="/img/linkedin.png" alt="linkedin" />
			</a>

			<a target="_blank" href="https://www.artstation.com/bachelerie_luc">
				<img className={classes.icon} src="/img/Artstation-512.png" alt="Artstation" />
			</a>
		</Grid>
	);
}

export default withStyles(styles)(NavPartage);
