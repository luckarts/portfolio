import React, { Component } from 'react';
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';
import { Button } from '@material-ui/core';
import './Gallery.css';
import { withBook } from '../context/Context';
import Lightbox from 'react-images';

const photos = [
	{
		src: '/img/copper_study.jpg',
		width: 4,
		height: 3
	},
	{
		src: '/img/study.jpg',
		width: 4,
		height: 3
	},
	{
		src: '/img/forest2.jpg',
		width: 3,
		height: 4
	},

	{
		src: '/img/concept_street_church1.jpg',
		width: 1,
		height: 1
	},
	{
		src: '/img/concept_street_church.jpg',
		width: 1,
		height: 1
	},
	{
		src: '/img/InspiredCampion.jpg',
		width: 4,
		height: 3
	},

	{
		src: '/img/painting_river_mark.jpg',
		width: 3,
		height: 4
	},
	{
		src: '/img/painting-lyly.jpg',
		width: 3,
		height: 4
	},

	{
		src: '/img/tableau.png',
		width: 4,
		height: 3
	},

	{
		src: '/img/study_color2.jpg',
		width: 3,
		height: 4
	}
];
class Imagenes extends Component {
	constructor() {
		super();
		this.state = {
			currentImage: 0
		};
	}
	openLightbox = obj => {
		this.setState({
			currentImage: obj,
			lightboxIsOpen: true
		});
	};
	closeLightbox = () => {
		this.setState({
			currentImage: 0,
			lightboxIsOpen: false
		});
	};
	gotoPrevious = () => {
		this.setState({
			currentImage: this.state.currentImage - 1
		});
	};
	gotoNext = () => {
		this.setState({
			currentImage: this.state.currentImage + 1
		});
	};
	render() {
		return (
			<div>
				<ResponsiveMasonry columnsCountBreakPoints={{ 350: 2, 750: 4, 900: 3 }}>
					<Masonry>
						{photos.map((photos, i) => (
							<div key={[i]}>
								<div style={{ padding: '7px' }}>
									<Button onClick={() => this.openLightbox(i)}>
										<img
											className={this.state.lightboxIsOpen === true ? 'gallery' : ''}
											key={[i]}
											src={photos.src}
											style={{
												width: '100%',
												display: 'block',
												marginBottom: '5px',
												borderRadius: '10px'
											}}
											alt="img"
										/>
									</Button>
								</div>
							</div>
						))}
					</Masonry>
				</ResponsiveMasonry>

				<Lightbox
					images={photos}
					currentImage={this.state.currentImage}
					onClose={this.closeLightbox}
					onClickPrev={this.gotoPrevious}
					onClickNext={this.gotoNext}
					isOpen={this.state.lightboxIsOpen}
				/>
			</div>
		);
	}
}
export default withBook(Imagenes);
