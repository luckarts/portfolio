import React from 'react';
import PropTypes from 'prop-types';

import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Background from '../../../public/img/forest2.jpg';
import character from '../../../public/img/character.jpg';
const styles = theme => ({
	main: {
		backgroundImage: `url(${Background})`,
		backgroundSize: 'cover',
		height: '60vh',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	bg: {
		width: '100%',
		height: '5vh',
		bottom: 0,
		background: `linear-gradient(to bottom,rgba(17, 17, 17,0), rgba(17, 17, 17))`
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	title: {
		alignItems: 'center',
		justifyContent: 'center',
		textAlign: 'center',
		paddingTop: theme.spacing.unit * 5,
		paddingBottom: theme.spacing.unit * 5,

		fontSize: '2em',
		width: '100%',
		color: '#eee'
	},
	character: {
		marginTop: '20vh',
		width: '120px',
		height: '120px',
		borderRadius: '100px'
	}
});

function SignIn(props) {
	const { classes } = props;

	return (
		<Grid container className={classes.main} direction="column">
			<Grid container xs={12} justify="center" alignItems="flex-end">
				<img className={classes.character} src={character} />
				<Grid />
				<Grid container xs={12} justify="center">
					<Typography className={classes.title} variant="h6" color="#eee" noWrap>
						Bachelerie Luc
					</Typography>
					<Grid />

					<Grid className={classes.bg} container xs={12} justify="center" alignItems="flex-end">
						<img />
						<Grid />
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	);
}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SignIn);
