import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid, Typography, Button } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from '../../jss/Intro';
import { withBook } from '../../context/Context';
import Progress from '../Progress';
import '../../jss/Intro.scss';
class SignIn extends Component {
	constructor(props) {
		super();
		this.state = {
			checked: true
		};
	}
	render() {
		const { classes } = this.props;

		return (
			<Grid container className={classes.main} direction="row">
				<Grid container item className={classes.welcome} align="center" xs={12} lg={12}>
					<img className="bachelart" src={'/img/LOGO7.png'} alt="logo bachelart" />
					<Typography component="h1" variant="h6" className="luc">
						Développeur Js
					</Typography>
					{this.props.width >= 1024 ? <Progress /> : ''}
					{this.props.width <= 1024 ? (
						<Button className="continuer">
							balayer<i className="material-icons">trending_flat</i>
						</Button>
					) : (
						''
					)}
				</Grid>
			</Grid>
		);
	}
}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withBook(withStyles(styles)(SignIn));
