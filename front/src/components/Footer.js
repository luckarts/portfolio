import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import { Grid, Paper } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Background from '../img/forest2.jpg';
const styles = theme => ({
	main: {
		backgroundImage: `url(${Background})`,
		backgroundSize: 'cover',
		height: '100vh',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1
		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit
	},
	submit: {
		marginTop: theme.spacing.unit * 3
	},
	welcome: {
		alignItems: 'center',
		justifyContent: 'center',
		textAlign: 'center'
		/* 	height: '20vh',
		paddingTop: theme.spacing.unit * 20 */
	},
	title: {
		alignItems: 'center',
		justifyContent: 'center',
		textAlign: 'center',
		paddingTop: theme.spacing.unit * 8,
		paddingBottom: theme.spacing.unit * 8,
		backgroundColor: 'rgba(17, 17, 17, 0.7) ',
		fontSize: '1em',
		width: '100%',
		color: '#eee'
	},
	footer: {
		position: 'absolute',

		bottom: 0,
		height: '10vh',
		width: '100%',
		background: `linear-gradient(to bottom,rgba(17, 17, 17,0), rgba(17, 17, 17))`
	}
});

function SignIn(props) {
	const { classes } = props;

	return (
		<footer className={classes.footer}>
			<Typography variant="h6" align="center" gutterBottom>
				Footer
			</Typography>
			<Typography variant="subtitle1" align="center" color="textSecondary" component="p">
				Something here to give the footer a purpose!
			</Typography>
		</footer>
	);
}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SignIn);
