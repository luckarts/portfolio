import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withBook } from '../context/Context';
import '../jss/Intro.scss';
const styles = theme => ({
	progress: {
		margin: theme.spacing.unit * 2,
		color: '#fff'
	}
});

class CircularDeterminate extends React.Component {
	state = {
		completed: 0
	};

	componentDidMount() {
		this.timer = setInterval(this.progress, 20);
	}

	cleare = () => {
		clearInterval(this.timer);
		this.props.activeTab(1);
	};

	progress = () => {
		const { completed } = this.state;
		this.setState({ completed: completed >= 170 ? 0 : completed + 1 });
		if (completed === 170) {
			return this.cleare();
		} else return;
	};

	render() {
		const { classes } = this.props;
		return (
			<div className="luc">
				<CircularProgress className={classes.progress} value={this.state.completed} />
			</div>
		);
	}
}

CircularDeterminate.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withBook(withStyles(styles)(CircularDeterminate));
