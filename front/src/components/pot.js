import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Grid, Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import CardPortfolio from './CardPortfolio';
import { withBook } from '../context/Context';
import styles from '../jss/Portfolio';

const Projets = [
	{ id: 0, title: 'Calypso', image: '/img/YkariIntro.jpg' },
	{ id: 1, title: 'Ikari', image: '/img/hikari.jpg' },
	{ id: 2, title: 'Spotter', image: '/img/Spotter.jpg' }
];
/* eslint-disable */
class SignIn extends Component {
	constructor(props) {
		super();
		this.state = {};
	}

	onClick = () => {
		console.log('hello');
	};
	render() {
		const { classes } = this.props;
		return (
			<Grid item container className={classes.main} justify="center">
				<Typography className={classes.title} variant="h6" noWrap>
					Portfolio
				</Typography>
				<Grid
					container
					className={classes.portfolio}
					item
					xs={12}
					spacing={40}
					sm={8}
					justify="center"
					direction="row"
				>
					{Projets.map((Projets, i) => (
						<Grid container item key={i} lg={12} xs={12}>
							<Button
								value={Projets.id}
								key={Projets.toString()}
								onClick={() => {
									this.props.activeTab(4);
								}}
							>
								{/* <img src={character.image} alt={character.name} /> */}

								<CardPortfolio image={Projets.image} />
							</Button>
						</Grid>
					))}
				</Grid>
			</Grid>
		);
	}
}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withBook(withStyles(styles)(SignIn));
