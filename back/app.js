const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const app = express();
//routes

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

// resolve Cors protection
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, Content-Type');
	res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
	next();
});

let server = app.listen(process.env.PORT || 80, function() {
	console.log('Listening on port ' + server.address().port);
});
