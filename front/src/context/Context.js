//etat global
import React, { Component, createContext } from 'react';
export const Context = createContext();
/* eslint-disable */
const IMAGES = [0, 1, 2, 3];
const RIGHT = -1;
const LEFT = 1;
const transitionDuration = 500;
class ContextProvider extends Component {
	constructor() {
		super();
		this.state = {
			tabs: 0,
			activeTab: this.handleChange,
			onSwiped: this.onSwiped,
			handleClick: this.handleClick,
			elementIn: true,
			handleOpen: this.handleOpen,
			handleClose: this.handleClose,
			open: false,
			width: window.innerWidth
		};
	}
	handleOpen = () => {

		this.setState({ open: true });
	};

	handleClose = () => {
		this.setState({ open: false });
	};
	handleChange = value => {

		this.setState({ tabs: value });
	};

	handleClick = value => {
		this.setState({
			elementIn: false
		});
		setTimeout(
			() => {
				this.setState({
					elementIn: true
				});
			},

			transitionDuration,
			this.handleChange(value)
		);
	};
	onSwiped = direction => {
		const change = direction === RIGHT ? RIGHT : LEFT;
		const adjustedIdx = this.state.tabs + Number(change);
		let newIdx;
		if (adjustedIdx >= IMAGES.length) {
			newIdx = 0;
		} else if (adjustedIdx < 0) {
			newIdx = IMAGES.length - 1;
		} else {
			newIdx = adjustedIdx;
		}
		if (newIdx === 0) {
			this.setState({ tabs: 1 });
		} else this.setState({ tabs: newIdx });
	};
	render() {
		return <Context.Provider value={this.state}>{this.props.children}</Context.Provider>;
	}
}
export default ContextProvider;

export const withBook = Component => props => (
	<Context.Consumer>{value => <Component {...props} {...value} />}</Context.Consumer>
);
