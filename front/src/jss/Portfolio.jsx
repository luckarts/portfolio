const styles = theme => ({
	main: {
		marginTop: '90px',
		backgroundColor: '#e7e6e3',
		paddingBottom: theme.spacing.unit * 5,
		backgroundSize: 'cover',
		height: '100%',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1,
		[theme.breakpoints.down('sm', 'xs')]: {
			marginBottom: '90px',
			marginTop: '0'
		}

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	bg: {
		width: '100%',
		height: '5vh',
		bottom: 0
		//background: `linear-gradient(to bottom,rgba(17, 17, 17,0), rgba(17, 17, 17))`
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	title: {
		color: '#213142',
		width: '50%',
		fontWeight: 'bold',
		textAlign: 'center',
		fontSize: '3em',

		borderBottom: `1px solid ${theme.palette.grey[500]}`,
		paddingTop: theme.spacing.unit * 4
	},
	summary: {
		paddingTop: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,

		fontSize: '1em',
		width: '100%',
		color: '#000',

		borderBottom: `1px solid ${theme.palette.grey[500]}`
		//paddingBottom: theme.spacing.unit * 5
		//borderBottom: `1px solid ${theme.palette.grey[500]}`
	},
	h2: {
		textAlign: 'center',
		fontSize: '1em',
		borderBottom: `1px solid ${theme.palette.grey[500]}`,
		paddingBottom: theme.spacing.unit * 5
	},

	h3: {
		paddingTop: theme.spacing.unit * 5,
		fontSize: '2em',
		width: '100%',
		color: '#000'
	},
	portfolio: { marginTop: theme.spacing.unit * 8 },
	character: {
		marginTop: '20vh',
		width: '150px',
		height: '150px',
		borderRadius: '300px'
	}
});
export default styles;
