import React from 'react';

import Button from '@material-ui/core/Button';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
/* eslint-disable */
const styles = theme => ({
	main: {
		marginBottom: theme.spacing.unit * 10,
		backgroundSize: 'cover',
		height: '90vh',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	bg: {
		width: '100%',
		height: '100%',
		bottom: 0,
		background: `#000`,
		paddingTop: theme.spacing.unit * 8,
		paddingBottom: theme.spacing.unit * 8
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	title: {
		paddingTop: theme.spacing.unit * 10,
		paddingBottom: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,
		textAlign: 'center',
		fontSize: '2em',
		width: '50%',
		color: '#000',
		borderBottom: `1px solid ${theme.palette.grey[500]}`
	},
	character: {
		//	marginRight: theme.spacing.unit * 10,

		width: '100%',
		height: '100%'
	},
	portfolio: { marginTop: theme.spacing.unit * 8 },
	text: { marginLeft: '50px' }
});

function Spotter(props) {
	const { classes } = props;

	return (
		<Grid item container justify="center" className={classes.paper}>
			<Typography className={classes.title} variant="h6" noWrap>
				Spotter
			</Typography>
			<Grid container item className={classes.portfolio} xs={12} sm={10} justify="center" direction="row">
				<Grid item container className={classes.bg} xs={12} sm={5} justify="center">
					<img className={classes.character} src={'/img/Spotter.jpg'} />
				</Grid>
				<Grid container item xs={12} sm={5} className={classes.text}>
					<Typography variant="h6">
						Passionné du numérique , après avoir travailler pendant 3 ans en tant que Graphiste 2D/3D . J'ai
						choisi de m'orienté vers le développement web . [Disponible à partir du 4 février] en tant que
						développeur web Fullstack Javascript / React avec la méthode Agile et Scrum .
						<Button>portfolio</Button>
					</Typography>
				</Grid>
			</Grid>
		</Grid>
	);
}

export default withStyles(styles)(Spotter);
