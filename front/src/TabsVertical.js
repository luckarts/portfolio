import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, Drawer, Divider, List, ListItem, ListItemIcon, Grid } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import SignIn from './components/SignIn';
import Profile from './components/Profile';
import AppBarBottom from './components/AppBarBottom';
import Gallery2D from './Gallery2D';
import classNames from 'classnames';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import Portfolio from './components/Portfolio';
import character from './img/character.jpg';
import './Tabs.css';
/* eslint-disable */
const numbers = [
	{ id: 0, img: { character }, title: 'Portfolio' },
	{ id: 1, logo: { character }, title: 'Portfolio' },
	{ id: 2, logo: { character }, title: 'Portfolio' }
];
function TabContainer(props) {
	return <Typography component="div">{props.children}</Typography>;
}

TabContainer.propTypes = {
	children: PropTypes.node.isRequired
};
const drawerWidth = '100%';
const styles = theme => ({
	root: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.paper
	},
	app: {
		display: 'flex'
	},

	drawer: {
		width: '5%',
		backgroundColor: '#eee',
		[theme.breakpoints.down('sm', 'xs')]: {
			display: 'none'
		}
	},
	list: { padding: 0 },
	appBar: {
		backgroundColor: '#eee',
		height: '100%',
		position: 'fixed',
		width: '5%',
		left: 0,
		justify: 'center',
		alignItems: 'flex-start',
		direction: 'culomn',
		[theme.breakpoints.down('sm', 'xs')]: {
			width: '100%',
			height: 'auto',
			direction: 'row',
			bottom: 0,
			justify: 'space-around',
			alignItems: 'center'
		}
	},
	toolBar: {
		justify: 'center !important',
		direction: 'row',
		[theme.breakpoints.down('sm', 'xs')]: {
			bottom: 0,
			justify: 'space-around',
			alignItems: 'center'
		}
	},
	character: {
		marginTop: theme.spacing.unit * 2,
		width: '50px',
		height: '50px',
		borderRadius: '300px'
	}
});

class SimpleTabs extends Component {
	state = {
		value: 0
		//key: [{ number: 0 }, { number: 1 }, { number: 2 }]
	};

	handleChange = (event, value) => {
		console.log({ value });
		this.setState({ value });
	};

	render() {
		const { classes } = this.props;
		const { value } = this.state;

		return (
			<div className={classes.root}>
				<AppBarBottom />
				<Grid container className={classes.appBar}>
					<Grid container className={classes.toolBar} justify="center">
						<img className={classes.character} src={character} />
						{numbers.map(number => (
							<Grid>
								<Tab
									className={classes.label}
									variant="fullWidth"
									key={number.toString()}
									value={number.id}
									id={number}
									label={number.title}
									onClick={this.handleChange}
								/>
							</Grid>
						))}
					</Grid>
				</Grid>
				{/* <AppBar className={classes.app}>
					<img className={classes.character} src={character} />
					<Tabs value={value} onChange={this.handleChange}>
						<Tab label="Item One" />
						<Tab label="Item Two" />
						<Tab label="Item Three" />
					</Tabs>
				</AppBar> */}
				{value === 0 && (
					<TabContainer>
						{' '}
						<Portfolio />
					</TabContainer>
				)}
				{value === 1 && (
					<TabContainer>
						<Profile />
					</TabContainer>
				)}
				{value === 2 && (
					<TabContainer>
						<Gallery2D />
					</TabContainer>
				)}
			</div>
		);
	}
}

SimpleTabs.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleTabs);
