const styles = theme => ({
	main: {
		padding: 0,
		backgroundImage: `url('/img/forest2.jpg')`,
		backgroundSize: 'cover',
		minHeight: '100vh',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1
		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit
	},
	submit: {
		marginTop: theme.spacing.unit * 3
	},
	welcome: {
		flexDirection: 'column',
		marginTop: '20vh',
		marginBottom: '30vh',
		alignItems: 'center',
		justifyContent: 'center',
		textAlign: 'center'
		//backgroundColor: 'rgba(17, 17, 17, 0.7) '

		/* 	height: '20vh',
		paddingTop: theme.spacing.unit * 20 */
	},
	title: {
		alignItems: 'center',
		justifyContent: 'center',
		textAlign: 'center',

		fontSize: '3em',
		width: '100%',
		color: '#eee',
		[theme.breakpoints.down('sm', 'xs')]: {}
	},
	footer: {
		position: 'absolute',

		bottom: 0,
		height: '10vh',
		width: '100%',
		background: `linear-gradient(to bottom,rgba(17, 17, 17,0), rgba(17, 17, 17))`
	},
	continuer: {
		backgroundColor: '#eee',
		color: '#000'
	},

});
export default styles;
