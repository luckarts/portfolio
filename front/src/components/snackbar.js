import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Slide from '@material-ui/core/Slide';
import { withBook } from '../context/Context';
function TransitionLeft(props) {
	return <Slide {...props} direction="left" />;
}

function TransitionUp(props) {
	return <Slide {...props} direction="up" />;
}

function TransitionRight(props) {
	return <Slide {...props} direction="right" />;
}

function TransitionDown(props) {
	return <Slide {...props} direction="down" />;
}

class DirectionSnackbar extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<Button onClick={this.props.hand(TransitionLeft)}>Right</Button>
				<Button onClick={this.props.hand(TransitionUp)}>Up</Button>
				<Button onClick={this.props.hand(TransitionRight)}>Left</Button>
				<Button onClick={this.props.hand(TransitionDown)}>Down</Button>
				<Snackbar
					open={this.props.open}
					TransitionComponent={this.props.Transition}
					ContentProps={{
						'aria-describedby': 'message-id'
					}}
					message={<span id="message-id">I love snacks</span>}
				/>
			</div>
		);
	}
}

export default withBook(DirectionSnackbar);
