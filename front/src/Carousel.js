import React from 'react';
import SwipeableViews from 'react-swipeable-views';

import AppBar from './components/AppBar';
import SignIn from './components/SignIn';
import Grid2D from './components/Grid2D';
import CardList from './components/CardList';
import Profile from './components/Profile';
import Controls from './Carouselcontrols';
/* eslint-disable */
const styles = {
	slide: {
		minHeight: 100,
		color: '#fff'
	},
	slide1: {},
	slide2: {},
	slide3: {}
};

class Carousell extends React.Component {
	state = {
		index: 1
	};

	handleChangeIndex = index => {
		this.setState({ index });
	};
	render() {
		const { index } = this.state;
		return (
			<div>
				<SwipeableViews enableMouseEvents resistance index={index} onChangeIndex={this.handleChangeIndex}>
					<SignIn />
					<Profile />
					<Grid2D />
				</SwipeableViews>
			</div>
		);
	}
}

export default Carousell;
