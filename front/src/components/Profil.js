import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import character from '../../public/img/character.jpg';
const styles = theme => ({
	main: {
		marginTop: theme.spacing.unit * 10,
		marginBottom: theme.spacing.unit * 10,
		backgroundSize: 'cover',
		height: '100%',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	bg: {
		width: '100%',
		height: '5vh',
		bottom: 0,
		background: `linear-gradient(to bottom,rgba(17, 17, 17,0), rgba(17, 17, 17))`
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	title: {
		paddingTop: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,

		fontSize: '2em',
		width: '100%',
		color: '#000',
		borderBottom: `1px solid ${theme.palette.grey[500]}`
	},
	character: {
		marginTop: '20vh',
		width: '200px',
		height: '200px',
		borderRadius: '300px'
	}
});

function SignIn(props) {
	const { classes } = props;

	return (
		<Grid container className={classes.main} direction="row">
			<Grid container xs={12} sm={5} justify="center">
				<img className={classes.character} src={character} />
			</Grid>
			<Grid container xs={12} sm={5} justify="flex-start" alignItems="center">
				<Typography className={classes.title} variant="h6" noWrap>
					Qui Suis Je ?
				</Typography>
				<Typography variant="h6">
					Passionné du numérique , après avoir travailler pendant 3 ans en tant que Graphiste 2D/3D . J'ai
					choisi de m'orienté vers le développement web . [Disponible à partir du 4 février] en tant que
					développeur web Fullstack Javascript / React avec la méthode Agile et Scrum .
				</Typography>
				<Button className={classes.login} color="black">
					portfolio
				</Button>
			</Grid>
		</Grid>
	);
}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SignIn);
