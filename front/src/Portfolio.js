import React, { Component } from 'react';
import AppBarBottom from './components/AppBarBottom';
import AppBar from './components/AppBar';
import Profil from './components/page1/Profiltest';
import Profile from './components/Profile';
import Table from './components/Table';
import Portfolios from './components/Portfolio';

import './jss/port.scss';
import Snackbar from './components/snackbar';
import { Grid, withStyles } from '@material-ui/core';

class App extends Component {
	render() {
		const { classes } = this.props;
		return (
			<div className="animation">
				<Grid container alignItems="center" className={classes.container}>
					<AppBarBottom />

					<Portfolios />
					<Snackbar />
					<AppBar />
				</Grid>
			</div>
		);
	}
}
export default withStyles(styles)(App);
