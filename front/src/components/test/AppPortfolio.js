import React from 'react';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Background from '../../img/forest2.jpg';

/* eslint-disable */
const styles = theme => ({
	main: {
		marginBottom: theme.spacing.unit * 10,
		backgroundSize: 'cover',
		height: '90vh',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	bg: {
		width: '100%',
		height: '100%',
		bottom: 0,
		background: `#000`,
		paddingTop: theme.spacing.unit * 8,
		paddingBottom: theme.spacing.unit * 8
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	title: {
		paddingTop: theme.spacing.unit * 5,
		paddingBottom: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,
		textAlign: 'center',
		fontSize: '2em',
		width: '50%',
		color: '#000',
		borderBottom: `1px solid ${theme.palette.grey[500]}`
	},
	character: {
		//	marginRight: theme.spacing.unit * 10,

		width: '100%',
		height: '100%'
	},
	portfolio: { marginTop: theme.spacing.unit * 8 }
});

function AppPortfolio(props) {
	const { classes } = props;

	return (
		<Grid justify="center" alignItems="center" className={classes.paper}>
			<Typography className={classes.title} alignItems="flex-end" variant="h6" color="#eee" noWrap>
				Potfolio
			</Typography>
			<Grid container className={classes.portfolio} xs={12} sm={10} justify="center" direction="row">
				<Grid container className={classes.bg} xs={12} sm={5} justify="center">
					<img className={classes.character} src={Background} />
				</Grid>
				<Grid container xs={12} sm={5}>
					<Typography variant="h6" color="#eee">
						Passionné du numérique , après avoir travailler pendant 3 ans en tant que Graphiste 2D/3D . J'ai
						choisi de m'orienté vers le développement web . [Disponible à partir du 4 février] en tant que
						développeur web Fullstack Javascript / React avec la méthode Agile et Scrum .
						<Button color="black">portfolio</Button>
					</Typography>
				</Grid>
			</Grid>
		</Grid>
	);
}

export default withStyles(styles)(AppPortfolio);
