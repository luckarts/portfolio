import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles, Drawer, Divider, List, ListItem, ListItemIcon, Button } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import SignIn from './components/intro/SignIn';
import Profile from './components/Profile';
import AppBarBottom from './components/AppBarBottom';
import Gallery2D from './Gallery2D';
import classNames from 'classnames';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import './Tabs.css';
/* eslint-disable */
const numbers = [0, 1, 2];
function TabContainer(props) {
	return <Typography component="div">{props.children}</Typography>;
}

TabContainer.propTypes = {
	children: PropTypes.node.isRequired
};
const drawerWidth = '100%';
const styles = theme => ({
	root: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.paper
	},
	appBar: {
		position: 'relative',
		marginTop: theme.spacing.unit * 8,
		display: 'none',
		bottom: 0,
		[theme.breakpoints.down('sm', 'xs')]: {
			display: 'block'
		}
	},

	drawer: {
		width: '5%',
		backgroundColor: '#eee',
		[theme.breakpoints.down('sm', 'xs')]: {
			display: 'none'
		}
	},
	list: { padding: 0 }
});

class SimpleTabs extends Component {
	state = {
		value: 0
		//key: [{ number: 0 }, { number: 1 }, { number: 2 }]
	};

	handleChange = (event, value) => {
		console.log({ value });
		this.setState({ value });
	};

	render() {
		const { classes } = this.props;
		const { value } = this.state;

		return (
			<div className={classes.root}>
				<AppBarBottom />
				<Drawer
					variant="permanent"
					classes={{
						paper: classNames(classes.drawer)
					}}
					open={this.state.open}
				>
					<List>
						{/* <ListItemIcon>
							<InboxIcon id={1} onClick={this.handleChange} />
							<InboxIcon id={2} onClick={this.handleChange} />
						</ListItemIcon> */}

						{numbers.map(number => (
							<ListItem className={classes.list}>
								<Tab
									classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
									key={value.toString()}
									value={number}
									id={number}
									label={number}
									onChange={this.handleChange}
								/>
							</ListItem>
						))}
					</List>
				</Drawer>
				<AppBar className={classes.appBar}>
					<Tabs value={value} onChange={this.handleChange}>
						<Tab label="Item One" />
						<Tab label="Item Two" />
						<Tab label="Item Three" />
					</Tabs>
				</AppBar>
				{value === 0 && (
					<TabContainer>
						{' '}
						<SignIn />
					</TabContainer>
				)}
				{value === 1 && (
					<TabContainer>
						<Profile />
					</TabContainer>
				)}
				{value === 2 && (
					<TabContainer>
						<Gallery2D />
					</TabContainer>
				)}
			</div>
		);
	}
}

SimpleTabs.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleTabs);
