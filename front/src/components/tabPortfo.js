import React from 'react';
import PropTypes from 'prop-types';
import { AppBar, Toolbar, Typography, IconButton, Button, Grid } from '@material-ui/core';

import withStyles from '@material-ui/core/styles/withStyles';
import Background from '../img/forest2.jpg';
import character from '../img/character.jpg';
const numbers = [0, 1, 2];
/* eslint-disable */
const styles = theme => ({
	main: {
		marginTop: theme.spacing.unit * 10,
		marginBottom: theme.spacing.unit * 10,
		backgroundSize: 'cover',
		height: '100%',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	bg: {
		width: '100%',
		height: '5vh',
		bottom: 0,
		background: `linear-gradient(to bottom,rgba(17, 17, 17,0), rgba(17, 17, 17))`
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	title: {
		paddingTop: theme.spacing.unit * 5,
		paddingBottom: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,
		textAlign: 'center',
		fontSize: '2em',
		width: '50%',
		color: '#000',
		borderBottom: `1px solid ${theme.palette.grey[500]}`
	},
	character: {
		marginRight: theme.spacing.unit * 10,

		width: '100%',
		height: '50%'
	},
	portfolio: { marginTop: theme.spacing.unit * 8 },
	appBar: {
		backgroundColor: '#ffb74d',
		height: '100%',
		position: 'fixed',
		width: '8%',
		left: 0,
		justify: 'center',
		alignItems: 'flex-start',
		[theme.breakpoints.down('sm', 'xs')]: {
			width: '100%',
			height: 'auto',
			bottom: 0,
			backgroundColor: '#2196f3',
			direction: 'row'
		}
	},
	toolBar: {
		direction: 'row',

		[theme.breakpoints.down('sm', 'xs')]: {
			direction: 'column',
			width: '100%',
			backgroundColor: '#2196f3'
		}
	},

	grow: {
		width: '100%',
		textAlign: 'center',
		flexGrow: 1,
		justify: 'center',
		alignItems: 'center'
	},
	menuButton: { width: '100%' },
	login: { width: '100%' }
});

function TabsPorfolio(props) {
	const { classes } = props;

	return (
		<Grid>
			<Grid container className={classes.appBar}>
				<Grid container className={classes.toolBar}>
					<Typography variant="h6" color="inherit" className={classes.grow}>
						News
					</Typography>
					<Button className={classes.login} color="inherit">
						Login
					</Button>
				</Grid>
			</Grid>
		</Grid>
	);
}

export default withStyles(styles)(TabsPorfolio);
