/* eslint-disable */
const styles = theme => ({
	root: {
		flexGrow: 1
	},
	app: {
		display: 'flex'
	},

	drawer: {
		width: '5%',
		backgroundColor: '#e7e6e3;',
		[theme.breakpoints.down('sm', 'xs')]: {
			display: 'none'
		}
	},
	list: { padding: 0 },
	appBar: {
		backgroundColor: '#242424',
		height: 'auto',
		position: 'fixed',
		width: '100%',
		left: 0,
		justify: 'space-around',

		direction: 'row',
		[theme.breakpoints.down('sm', 'xs')]: {
			width: '100%',
			height: 'auto',
			direction: 'row',
			bottom: 0,
			justify: 'space-around',
			alignItems: 'center'
		}
	},
	toolBar: {
		marginTop: theme.spacing.unit * 1,
		marginBottom: theme.spacing.unit * 1,
		width: '80%',
		justify: 'space-around',
		direction: 'row',
		[theme.breakpoints.down('sm', 'xs')]: {
			bottom: 0,
			justify: 'space-around',
			alignItems: 'center'
		}
	},
	character: {
		width: 'auto',
		height: '50px',
		borderRadius: '300px'
	},

	label: { color: '#eee', fontSize: '1em', fontWeight: 'Bold' }
});
export default styles;
