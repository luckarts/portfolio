import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Typography, Chip, CardContent } from '@material-ui/core';
import '../jss/profile.scss';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from '../jss/Profile';
import Navpartage from './NavPartage';
function SignIn(props) {
	const { classes } = props;

	return (
		<Grid container className={classes.main} justify="center">
			<Grid className={classes.bg}>
				<Grid item container xs={12} justify="center">
					<img className={classes.character} src={'/img/character.jpg'} alt="img" />
				</Grid>

				<Grid item container xs={12} sm={12} justify="center" direction="column">
					<Typography className={classes.title} variant="h6" noWrap>
						Bachelerie Luc
					</Typography>

					<Typography className={classes.h5} variant="h6">
						Develloppeur Front end Javascript <br />
					</Typography>
					<Typography className={classes.h6} variant="h6">
						Nice / Lyon
					</Typography>

					<Navpartage />
				</Grid>
			</Grid>
			<Grid item container xs={12} sm={7} className={classes.summary}>
				<div className={classes.card}>
					<CardContent>
						<Typography className={classes.h3} variant="h6" noWrap>
							Sommaire
						</Typography>
						<Typography className={classes.h4} variant="h6">
							Passionné du numérique , après avoir travailler pendant 3 ans en tant que Graphiste 2D/3D .
							J'ai choisi de m'orienté vers le développement web . [Disponible à partir du 4 février] en
							tant que développeur web Fullstack Javascript / React avec la méthode Agile et Scrum .
						</Typography>
					</CardContent>
				</div>
			</Grid>

			<Grid item container xs={12} sm={7} className={classes.summary}>
				<div className={classes.card}>
					<CardContent>
						<Typography className={classes.h3} variant="h6" noWrap>
							Skills
						</Typography>
						<Chip
							color="primary"
							variant="outlined"
							label="React / Redux / Context"
							className={classes.chip}
						/>
						<Chip color="primary" variant="outlined" label="Angular" className={classes.chip} />
						<Chip color="primary" variant="outlined" label="NodeJs" className={classes.chip} />
						<Chip
							color="primary"
							variant="outlined"
							label="SQL / NoSQL / MongoD"
							className={classes.chip}
						/>
						<Chip color="primary" variant="outlined" label="Git / GitLab" className={classes.chip} />
						<Chip color="primary" variant="outlined" label="TDD / Agile / Scrum" className={classes.chip} />
						<Chip color="primary" variant="outlined" label="Javascript ES6" className={classes.chip} />
						<Chip color="primary" variant="outlined" label="Html5 / Css / Sass" className={classes.chip} />
					</CardContent>
				</div>
			</Grid>
		</Grid>
	);
}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SignIn);
