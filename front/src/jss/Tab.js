const styles = theme => ({
	root: {
		flexGrow: 1,
		padding: 0,
		backgroundColor: '#e7e6e3;',
		trackMouse: false,
		[theme.breakpoints.down('sm', 'xs')]: {
			trackMouse: true
		}
	},
	app: {
		display: 'flex'
	},

	drawer: {
		width: '5%',
		backgroundColor: '#e7e6e3;',
		[theme.breakpoints.down('sm', 'xs')]: {
			display: 'none'
		}
	},
	list: { padding: 0 },
	appBar: {
		backgroundColor: '#eee',
		height: '100%',
		position: 'fixed',
		width: '5%',
		left: 0,
		justify: 'center',
		alignItems: 'flex-start',
		direction: 'culomn',
		[theme.breakpoints.down('sm', 'xs')]: {
			width: '100%',
			height: 'auto',
			direction: 'row',
			bottom: 0,
			justify: 'space-around',
			alignItems: 'center'
		}
	},
	toolBar: {
		justify: 'center !important',
		direction: 'row',
		[theme.breakpoints.down('sm', 'xs')]: {
			bottom: 0,
			justify: 'space-around',
			alignItems: 'center'
		}
	},
	character: {
		marginTop: theme.spacing.unit * 2,
		width: '50px',
		height: '50px',
		borderRadius: '300px'
	},
	floatRight: { floatRight: 'right', marginBottom: '10vh' },
	floatLeft: { floatLeft: 'left', marginBottom: '10vh' }
});
export default styles;
