import React, { Component } from 'react';

import Grid2D from './components/Grid2D';

import './App.css';
import { Grid, withStyles, Typography } from '@material-ui/core';
/* eslint-disable */
const styles = theme => ({
	container: {
		marginTop: '90px',
		[theme.breakpoints.down('sm', 'xs')]: {
			marginBottom: '90px',
			marginTop: '0'
		},
		//backgroundImage: `url(${Background})`
		//backgroundColor: 'rgba(17, 17, 17)'
		backgroundColor: '#e7e6e3;'
	},
	grid: {
		marginRight: theme.spacing.unit * 10,
		marginLeft: theme.spacing.unit * 10,
		paddingTop: theme.spacing.unit * 5,
		[theme.breakpoints.down('sm', 'xs')]: {
			marginRight: 0,
			marginLeft: 0,
			paddingTop: theme.spacing.unit * 2
		}
	},
	portfolio: {
		color: '#213142',
		width: '50%',
		fontWeight: 'bold',
		textAlign: 'center',
		fontSize: '3em',
		marginBottom: theme.spacing.unit * 5,
		borderBottom: `1px solid ${theme.palette.grey[500]}`,
		paddingTop: theme.spacing.unit * 5
	}
});

class Gallery2D extends Component {
	render() {
		const { classes } = this.props;
		return (
			<Grid container className={classes.container} justify="center" alignItems="center">
				{/* 	<Grid container xs={12} sm={5} justify="center">
					<img className={classes.character} src={character} />
				</Grid>
				<Grid container xs={12} sm={5} justify="flex-start" alignItems="center">
					<Typography className={classes.title} variant="h6" color="#eee" noWrap>
						Qui Suis Je ?
					</Typography>
				</Grid> */}
				<Typography className={classes.portfolio} variant="h6">
					Galerie
				</Typography>
				<Grid className={classes.grid}>
					<Grid2D />
				</Grid>
			</Grid>
		);
	}
}

export default withStyles(styles)(Gallery2D);
