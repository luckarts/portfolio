const styles = theme => ({
	text: {
		paddingTop: theme.spacing.unit * 2,
		paddingLeft: theme.spacing.unit * 2,
		paddingRight: theme.spacing.unit * 2
	},
	paper: {
		position: 'absolute',
		width: theme.spacing.unit * 50,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing.unit * 4,
		outline: 'none'
	},
	list: {
		marginBottom: theme.spacing.unit * 2
	},
	subHeader: {
		backgroundColor: theme.palette.background.paper
	},
	appBar: {
		width: '100%',
		//background: `linear-gradient(to top,rgba(17, 17, 17,0), rgba(17, 17, 17))`,
		//backgroundColor: '#eee',
		//backgroundColor: '#242424',
		backgroundColor: '#213142',
		top: 0,
		[theme.breakpoints.down('sm', 'xs')]: {
			width: '100%',
			top: 'auto',
			bottom: 0,
			height: 'auto',
			direction: 'row'
		}
	},
	appBarShift: {
		//bottom: 0
	},
	toolbar: {
		//shadow: (theme.shadows = []),

		alignItems: 'center',
		justifyContent: 'space-between'

		//borderBottom: `1px solid ${theme.palette.grey[300]}`
	},

	login: { color: '#fff', fontWeight: 'bold' },

	character: {
		marginTop: theme.spacing.unit * 2,
		width: '50px',
		height: '50px',
		borderRadius: '300px'
	},
	bachelart: {
		marginTop: theme.spacing.unit * 2,
		marginLeft: theme.spacing.unit * 2,
		width: 'auto',
		height: '30px',
		display: 'block',
		[theme.breakpoints.down('sm', 'xs')]: {
			display: 'none'
		}
	},
	navbarTitle: { display: 'flex', width: '100%', textAlign: 'center' }
});
export default styles;
