import React from 'react';
import { AppBar, Toolbar, Typography, IconButton, Button, Grid } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import red from '@material-ui/core/colors/red';
const /* [theme.breakpoints.between('sm', 'md')]: {

}, */ styles = theme => ({
		root: {
			flexGrow: 1
		},
		appBar: {
			backgroundColor: '#ffb74d',
			height: '100%',
			position: 'fixed',
			width: '8%',
			left: 0,
			justify: 'center',
			alignItems: 'flex-end',
			[theme.breakpoints.down('sm', 'xs')]: {
				width: '100%',
				height: 'auto',
				bottom: 0,
				backgroundColor: '#2196f3',
				direction: 'row'
			}
		},
		toolBar: {
			direction: 'row',

			[theme.breakpoints.down('sm', 'xs')]: {
				direction: 'column',
				width: '100%',
				backgroundColor: '#2196f3'
			}
		},

		grow: {
			width: '100%',
			textAlign: 'center',
			flexGrow: 1,
			justify: 'center',
			alignItems: 'center'
		},
		menuButton: { width: '100%' },
		login: { width: '100%' }
	});

const NavBar = props => {
	const { classes } = props;
	return (
		<div className={classes.root}>
			<Grid container className={classes.appBar}>
				<Grid container className={classes.toolBar}>
					<IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
						<MenuIcon />
					</IconButton>
					<Typography variant="h6" color="inherit" className={classes.grow}>
						News
					</Typography>
					<Typography variant="h6" color="inherit" className={classes.grow}>
						News
					</Typography>
					<Button className={classes.login} color="inherit">
						Login
					</Button>
				</Grid>
			</Grid>
		</div>
	);
};

NavBar.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(NavBar);
