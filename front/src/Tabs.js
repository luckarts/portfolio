/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tabs, NoSsr, Tab, Typography, AppBar, withStyles, Grid, Button } from '@material-ui/core';
import AppBarBottom from './components/AppBarBottom';
import Portfolio from './components/Portfolio';
import Profile from './components/Profile';
import styles from './jss/Tab';
import Gallery2D from './Gallery2D';
import { withBook } from './context/Context';
import Swipeable from 'react-swipeable';
import Dashboard from './Dashboard';
import SwipeableViews from 'react-swipeable-views';
import './jss/tabs.scss';
const IMAGES = [0, 1, 2];
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
const RIGHT = -1;
const LEFT = 1;
const numbers = [
	{ id: 0, img: '/img/character.jpg', title: 'Portfolio' },
	{ id: 1, logo: '/img/character.jpg', title: 'Portfolio' },
	{ id: 2, logo: '/img/character.jpg', title: 'Portfolio' }
];
const transitionDuration = 500;
function TabContainer(props) {
	return <Typography component="div">{props.children}</Typography>;
}

styles.tabsWrapper = {
	transition: 'transform .3s ease',
	display: 'flex',
	willChange: 'transform'
};
TabContainer.propTypes = {
	children: PropTypes.node.isRequired
};

function LinkTab(props) {
	return <Tab component="a" onClick={event => event.preventDefault()} {...props} />;
}

class NavTabs extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = { slideStyle: 'slide', tabs: 0, trackMouse: false };
	}
	myAvatar = () => {
		if (this.props.tabs !== 1) {
			return <AppBarBottom />;
		} else {
			return;
		}
	};

	render() {
		const { classes } = this.props;
		const { tabs } = this.props;
		if (isWidthUp('sm', 'xs')) {
			this.setState({ trackMouse: true });
		}
		return (
			<NoSsr>
				{this.props.tabs !== 0 ? <AppBarBottom /> : ''}

				<div className={classes.root}>
					<SwipeableViews enableMouseEvents>
						<Swipeable
							trackMouse={this.state.trackMouse}
							onSwipedLeft={() => this.props.onSwiped(RIGHT)}
							onSwipedRight={() => this.props.onSwiped(LEFT)}
						>
							{/* <AppBar position="static">
						<Tabs variant="fullWidth" value={value} onChange={this.handleChange}>
							<LinkTab label="Page One" href="page1" />
							<LinkTab label="Page Two" href="page2" />
							<LinkTab label="Page Three" href="page3" />
						</Tabs>
					</AppBar> */}

							{tabs === 0 && (
								<TabContainer key={1}>
									<Dashboard />
								</TabContainer>
							)}
							{tabs === 1 && (
								<TabContainer key={2}>
									<Portfolio />
								</TabContainer>
							)}
							{tabs === 2 && (
								<TabContainer key={3}>
									<Profile />
								</TabContainer>
							)}
							{tabs === 3 && (
								<TabContainer key={4}>
									<Gallery2D />
								</TabContainer>
							)}
						</Swipeable>
					</SwipeableViews>
				</div>
			</NoSsr>
		);
	}
}
/* <Button onClick={() => this.onSwiped(RIGHT)} className={classe					⇦
				</Button>
				<Button onClick={() => this.onSwiped(LEFT)} className={classes.floatRight}>
					⇨
				</Button>
				<Controls itemsnumber={numbers.length} /> */
NavTabs.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withBook(withStyles(styles)(NavTabs));
