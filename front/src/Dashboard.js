import React, { Component } from 'react';

import SignIn from './components/intro/SignIn';

import './jss/App.scss';
import { withBook } from './context/Context';
import { Grid, withStyles } from '@material-ui/core';
const styles = theme => ({
	container: {
		//backgroundImage: `url(${Background})`
		//backgroundColor: 'rgba(17, 17, 17)'
		backgroundColor: '#e7e6e3;'
	}
});
/*
<AppBarBottom />
 <SignIn />
				<Profil />
				<CardList /> */
class App extends Component {
	render() {
		const { classes } = this.props;
		return (
			<div background-color="#000">
				<div className={this.props.width >= 1024 ? 'animation' : ''}>
					<Grid container alignItems="center" className={classes.container}>
						<SignIn />
					</Grid>
				</div>
			</div>
		);
	}
}
export default withBook(withStyles(styles)(App));
