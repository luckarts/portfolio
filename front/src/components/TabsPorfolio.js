import React from 'react';
import { Button, Grid } from '@material-ui/core';

import withStyles from '@material-ui/core/styles/withStyles';

/* eslint-disable */
const styles = theme => ({
	main: {
		marginTop: theme.spacing.unit * 10,
		marginBottom: theme.spacing.unit * 10,
		backgroundSize: 'cover',
		height: '100%',
		width: '100%',
		display: 'flex', // Fix IE 11 issue.
		flexGrow: 1

		//[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {}
	},
	bg: {
		width: '100%',
		height: '5vh',
		bottom: 0,
		background: `linear-gradient(to bottom,rgba(17, 17, 17,0), rgba(17, 17, 17))`
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
		//	padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
	},

	title: {
		paddingTop: theme.spacing.unit * 5,
		paddingBottom: theme.spacing.unit * 5,
		//paddingBottom: theme.spacing.unit * 5,
		textAlign: 'center',
		fontSize: '2em',
		width: '50%',
		color: '#000',
		borderBottom: `1px solid ${theme.palette.grey[500]}`
	},
	character: {
		marginRight: theme.spacing.unit * 10,

		width: '100%',
		height: '50%'
	},
	portfolio: { marginTop: theme.spacing.unit * 8 },
	appBar: {
		backgroundColor: '#eee',
		height: '100%',
		position: 'fixed',
		width: '5%',
		left: 0,
		justify: 'center',
		alignItems: 'flex-start',
		direction: 'culomn',
		[theme.breakpoints.down('sm', 'xs')]: {
			width: '100%',
			height: 'auto',
			direction: 'row',
			bottom: 0,
			justify: 'space-around',
			alignItems: 'center'
		}
	},
	toolBar: {
		justify: 'center !important',
		direction: 'row',
		[theme.breakpoints.down('sm', 'xs')]: {
			bottom: 0,
			justify: 'space-around',
			alignItems: 'center'
		}
	}
});

function TabsPorfolio(props) {
	const { classes } = props;

	return (
		<Grid container className={classes.appBar}>
			<Grid container className={classes.toolBar} justify="center">
				<Button variant="contained" className={classes.button}>
					Logo
				</Button>
				<Button variant="contained" className={classes.button}>
					Default
				</Button>
				<Button variant="contained" className={classes.button}>
					Default
				</Button>
			</Grid>
		</Grid>
	);
}

export default withStyles(styles)(TabsPorfolio);
