import React, { Component } from 'react';
import AppBarBottom from './components/AppBarBottom';
import SignIn from './components/SignIn';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import './App.css';
import { Grid, withStyles } from '@material-ui/core';
const styles = theme => ({
	container: {
		height: '100%',
		backgroundColor: '#404040'
	}
});
//Caroline de montillet sqli
class App extends Component {
	render() {
		const { classes } = this.props;
		return (
			<div>
				<MuiThemeProvider>
					<Grid container alignItems="center" className={classes.container}>
						<AppBarBottom />
						<SignIn />
					</Grid>
				</MuiThemeProvider>
			</div>
		);
	}
}
export default withStyles(styles)(App);
