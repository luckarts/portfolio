import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import { Button, Toolbar, withStyles, CssBaseline, Grid } from '@material-ui/core';
import { withBook } from '../context/Context';
import styles from '../jss/AppBar';
import '../jss/AppBar.scss';

/* eslint-disable */

const numbers = [0, 1, 2];
class BottomAppBar extends Component {
	constructor() {
		super();
		this.state = {
			open: false
		};
	}
	rand = () => {
		return Math.round(Math.random() * 20) - 10;
	};

	handleOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
		this.setState({ open: false });
	};

	render() {
		const { classes } = this.props;
		return (
			<div>
				<CssBaseline />

				<AppBar position="fixed" color="primary" className={classes.appBar}>
					<Toolbar>
						<Grid className={classes.toolbar}>
							<Button selected={this.props.tabs === 0} className={classes.menuButton} color="inherit">
								<img className={classes.character} src={'/img/logo8.png'} />
								<img className={classes.bachelart} src={'/img/LOGO7.png'} />
							</Button>
						</Grid>
						<Grid
							container
							className={classes.menu}
							direction="row"
							justify="space-around"
							alignItems="center"
						>
							<Button
								style={
									this.props.tabs === 1
										? {
												borderBottom: `2px solid red`
										  }
										: {}
								}
								className={classes.login}
								//className={this.props.tabs === 1 ? 'active' : ''}
								selected={this.props.tabs === 1}
								onClick={() => {
									this.props.activeTab(1);
								}}
							>
								Projets
							</Button>
							<Button
								style={
									this.props.tabs === 2
										? {
												borderBottom: `2px solid red`
										  }
										: {}
								}
								className={classes.login}
								//	className={this.props.tabs === 2 ? 'active' : ''}
								onClick={() => {
									this.props.activeTab(2);
								}}
							>
								Resume
							</Button>

							<Button
								style={
									this.props.tabs === 3
										? {
												borderBottom: `2px solid red`
										  }
										: {}
								}
								className={classes.login}
								//	className={this.props.tabs === 3 ? 'active' : ''}
								onClick={() => {
									this.props.activeTab(3);
								}}
							>
								Galerie
							</Button>
						</Grid>
					</Toolbar>
				</AppBar>
			</div>
		);
	}
}

BottomAppBar.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withBook(withStyles(styles)(BottomAppBar));

/* <Button className={classes.login} onClick={this.handleOpen}>
							Se Connecter
						</Button>
							<Modal
							aria-labelledby="simple-modal-title"
							aria-describedby="simple-modal-description"
							open={this.state.open}
							onClose={this.handleClose}
						>
							<Login />
						</Modal> */
